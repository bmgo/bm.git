package bm

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
)

func Res(c *gin.Context, code int, msg string, obj interface{}) {
	res := gin.H{
		"code": code,
		"msg":  msg,
	}

	if obj != nil && !reflect.ValueOf(obj).IsZero() {
		res["data"] = obj
	}
	c.JSON(http.StatusOK, res)
}

func ResData(c *gin.Context, obj interface{}) {
	Res(c, 0, "成功", obj)
}

func ResSuccess(c *gin.Context) {
	ResData(c, nil)
}
