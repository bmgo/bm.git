package bm

import (
	"fmt"
	"testing"
)

// @Title 用户
type UserApi struct {
	BmApi
}

type User struct {
	Id       int    `gorm:"primaryKey" description:"用户id"`
	Phone    string `gorm:"column(phone);size(255);null" json:"phone" description:"手机号"`
	UserName string `gorm:"column(user_name);size(100);null" json:"userName" description:"账号"`
	Name     string `gorm:"column(name);size(50);null" json:"name" description:"姓名"`
	NickName string `gorm:"column(nick_name);size(255);null" json:"nickName" description:"昵称"`
	Sex      string `gorm:"column(sex);size(10);null" json:"sex" description:"性别"`
	Email    string `gorm:"column(email);size(50);null" json:"email" description:"邮箱"`
	Password string `gorm:"column(password);size(100);null" json:"password" description:"密码"`
	Pic      string `gorm:"column(pic);size(255);null" json:"pic" description:"用户头像"`
	Status   int8   `gorm:"column(status);null" json:"status" description:"状态()"`
}

// @Title 用户列表
// @Router / [get]
func (UserApi) ListUser() []User {
	user := User{Id: 1, Phone: "18511111111", Name: "张三"}
	return []User{user}
}

// @Title 用户查看
// @Router /:id [get]
func (UserApi) GetUser(id int) (m User, e error) {
	fmt.Println("id", id)
	user := User{Id: 1, Phone: "18511111111", Name: "张三"}
	return user, nil
}

// @Title 用户新增
// @Router / [post]
func (UserApi) AddUser(req *User) int {
	fmt.Println(req)
	return req.Id
}

// @Title 用户修改
// @Router /:id [put]
func (UserApi) EditUser(id int, req *User) int {
	fmt.Println(req)
	return id
}

// @Title 用户删除
// @Router /:id [delete]
func (UserApi) DeleteUser(id int) {
	fmt.Println(id)
}

func TestRouter(t *testing.T) {
	r := NewRouter()
	r.Use(Recover)
	rApi := r.BmGroup("api")
	{
		rApi.BmApi("user", &UserApi{})
	}

	_ = r.Run(":8080")
	//localhost:8080/api/user 		列表[GET]
	//localhost:8080/api/user/:id	查询[GET]
	//localhost:8080/api/user	    新增[POST] 表单测试数据 {"Phone":"testPhone","UserName1":"testUserName","Name":"testName","NickName":"testNickName"}
	//localhost:8080/api/user/:id   修改[PUT]
	//localhost:8080/api/user/:id	删除[DELETE]
}
