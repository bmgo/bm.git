package bm

import (
	"reflect"
	"strconv"
)

type PathInt = int
type PathInta PathInt
type PathInt8 = int8
type PathInt16 = int16
type PathInt32 = int32
type PathInt64 = int64
type PathUint = uint
type PathUint8 = uint8
type PathUint16 = uint16
type PathUint32 = uint32
type PathUint64 = uint64
type PathFloat32 = float32
type PathFloat64 = float64
type PathStr = string
type PathBool = bool

type T struct {
	reflect.Type
}
type Te struct {
	PathInt
	PathInt8
	PathInt16
	PathInt32
	PathInt64
	PathUint
	PathUint8
	PathUint16
	PathUint32
	PathUint64
	PathFloat32
	PathFloat64
	PathStr
	PathBool
}

// string转基本类型（包含int、uint、float、bool及相关的8、16、32、64位），否则会失败
// var 值
// 需要转换的类型
func StrToType(val string, kind reflect.Kind) interface{} {
	switch kind {
	case reflect.Int:
		intVal, _ := strconv.Atoi(val)
		return intVal
	case reflect.Int8:
		intVal, _ := strconv.ParseInt(val, 10, 8)
		return int8(intVal)
	case reflect.Int16:
		intVal, _ := strconv.ParseInt(val, 10, 16)
		return int16(intVal)
	case reflect.Int32:
		intVal, _ := strconv.ParseInt(val, 10, 32)
		return int32(intVal)
	case reflect.Int64:
		intVal, _ := strconv.ParseInt(val, 10, 64)
		return intVal
	case reflect.Uint:
		uintVal, _ := strconv.ParseUint(val, 10, 0)
		return uint(uintVal)
	case reflect.Uint8:
		uintVal, _ := strconv.ParseUint(val, 10, 8)
		return uint8(uintVal)
	case reflect.Uint16:
		uintVal, _ := strconv.ParseUint(val, 10, 16)
		return uint16(uintVal)
	case reflect.Uint32:
		uintVal, _ := strconv.ParseUint(val, 10, 32)
		return uint32(uintVal)
	case reflect.Uint64:
		uintVal, _ := strconv.ParseUint(val, 10, 64)
		return uintVal
	case reflect.Bool:
		boolVal, _ := strconv.ParseBool(val)
		return boolVal
	case reflect.Float32:
		floatVal, _ := strconv.ParseFloat(val, 32)
		return float32(floatVal)
	case reflect.Float64:
		floatVal, _ := strconv.ParseFloat(val, 64)
		return floatVal
	case reflect.String:
		return val
	//case reflect.Struct:
	default:
		return nil
	}
}
