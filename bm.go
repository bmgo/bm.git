package bm

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
	"runtime"
	"strings"
)

type BmEngine struct {
	*gin.Engine
	*BmRouterGroup
}

type BmApi struct {
}

// RecoverErrorFunc recover 错误设置
type RecoverErrorFunc func(interface{})

// IRoutes defines all router handle interface.
type IBmRoutes interface {
	BmHandle(string, string, interface{}) IBmRoutes
	BmAny(string, interface{}) IBmRoutes
	BmGet(string, interface{}) IBmRoutes
	BmPost(string, interface{}) IBmRoutes
	BmDelete(string, interface{}) IBmRoutes
	BmPatch(string, interface{}) IBmRoutes
	BmPut(string, interface{}) IBmRoutes
	BmOptions(string, interface{}) IBmRoutes
	BmHead(string, interface{}) IBmRoutes
}

type BmRouterGroup struct {
	*gin.RouterGroup
}

func NewRouter() *BmEngine {
	g := gin.Default()
	//g := gin.New()
	r := &BmEngine{g, NewGroup(g)}
	return r
}

func NewGroup(e *gin.Engine) *BmRouterGroup {
	return &BmRouterGroup{&e.RouterGroup}
}

func (group *BmRouterGroup) BmGroup(relativePath string) *BmRouterGroup {
	ginGroup := group.Group(relativePath)
	return &BmRouterGroup{ginGroup}
}

// HandlerFunc Get and filter the parameters to be bound
func (group *BmRouterGroup) handle(httpMethod, relativePath string, handlerFunc interface{}, typeMethod ...reflect.Method) IBmRoutes { // 获取并过滤要绑定的参数
	method, ok := handlerFunc.(reflect.Value)
	var fullPath string
	if ok {
		tm := typeMethod[0].Func
		fullPath = runtime.FuncForPC(tm.Pointer()).Name()
	} else {
		method = reflect.ValueOf(handlerFunc)
		fullPath = runtime.FuncForPC(method.Pointer()).Name()
	}

	if !(method.Kind() == reflect.Func) {
		panic("路由必须是func, 当前为：" + method.Kind().String())
	}
	//fmt.Println("fullPath",fullPath)

	firstPoint := strings.Index(fullPath, ".")
	fullPath = strings.ReplaceAll(fullPath, "*", "")
	fullPath = strings.ReplaceAll(fullPath, "(", "")
	fullPath = strings.ReplaceAll(fullPath, ")", "")
	fullPath = strings.ReplaceAll(fullPath, "-fm", "")
	methodPath := fullPath[:firstPoint]
	methodName := fullPath[firstPoint+1:]
	docModel := GetDocModel(methodPath, methodName)
	//fmt.Println("docModel",docModel)

	if docModel == nil {
		return group
	}

	mType := method.Type()
	reqNum := mType.NumIn()   //入参数量
	respNum := mType.NumOut() //出参数量
	var rHandlerFunc gin.HandlerFunc
	//fmt.Println(methodName,reqNum,respNum)

	//gin默认参数
	rHandlerFunc = func(c *gin.Context) {
		//contentType := c.GetHeader("Content-Type")
		//fmt.Println("contentType",contentType)
		//入参处理
		ins := make([]reflect.Value, reqNum)
		//fmt.Println("reqNum",reqNum)
		for i := 0; i < reqNum; i++ {
			iType := mType.In(i)
			kind := iType.Kind()
			elemKind := kind
			if reflect.Ptr == kind {
				elemKind = iType.Elem().Kind()
			}

			if reflect.TypeOf(&gin.Context{}) == iType { //gin 默认对象
				ins[i] = reflect.ValueOf(c)
			} else if reflect.Struct == elemKind || //结构
				reflect.Array == elemKind || //数组
				reflect.Chan == elemKind ||
				reflect.Map == elemKind ||
				reflect.Slice == elemKind {
				if reflect.Ptr == kind {
					req := reflect.New(iType.Elem())
					_ = c.Bind(req.Interface())
					ins[i] = req
				} else {
					req := reflect.New(iType)
					_ = c.Bind(req.Interface())
					ins[i] = req.Elem()
				}
			} else {
				fi := docModel.Req[i]
				fieldName := fi.Name
				//fmt.Println(iType.String(), runtime.FuncForPC(reflect.ValueOf(method.In(0)).Pointer()).Name(),methodType)
				if fieldName == "" {
					panic("不能没有参数名：" + runtime.FuncForPC(reflect.ValueOf(handlerFunc).Pointer()).Name())
				}
				val, getOk := c.Params.Get(fieldName)
				if !getOk {
					val, getOk = c.GetQuery(fieldName)
					if !getOk {
						//fmt.Println(fieldName,httpMethod)
						if http.MethodGet != httpMethod {
							val = c.PostForm(fieldName)
						}
					}
				}

				//fmt.Println(fi.Name)
				ins[i] = reflect.ValueOf(StrToType(val, elemKind))
			}
		}
		//调用函数
		rs := method.Call(ins)
		//出参处理
		mapOut := make(map[string]interface{})
		var arryOut []interface{}
		var lastName string
		for i := 0; i < respNum; i++ {
			rsObj := rs[i].Interface()
			oType := mType.Out(i)
			kind := oType.Kind()
			elemKind := kind
			if reflect.Ptr == kind {
				elemKind = oType.Elem().Kind()
			}
			if "error" == oType.Name() && reflect.Interface == elemKind {
				if rsObj != nil {
					Res(c, 1, "失败", ErrorToString(rsObj))
					return
				}
			} else {
				fo := docModel.Resp[i]
				//fmt.Println("fo.Name", fo.Name, oType.Name())
				lastName = fo.Name
				if fo.Name == "" {
					arryOut = append(arryOut, rsObj)
				} else {
					mapOut[fo.Name] = rsObj
				}
			}
		}

		mn := len(mapOut)
		an := len(arryOut)
		if 1 == mn {
			ResData(c, mapOut[lastName])
		} else if 1 == len(arryOut) {
			ResData(c, arryOut[0])
		} else if 1 < mn {
			ResData(c, mapOut)
		} else if 1 < an {
			ResData(c, arryOut)
		} else {
			ResSuccess(c)
		}
	}
	//panic("method " + runtime.FuncForPC(reflect.ValueOf(handlerFunc).Pointer()).Name() + " not support!")

	group.Handle(httpMethod, relativePath, rHandlerFunc)
	return group
}

func (group *BmRouterGroup) BmHandle(httpMethod, relativePath string, handlerFunc interface{}) IBmRoutes {
	return group.handle(httpMethod, relativePath, handlerFunc)
}

func (group *BmRouterGroup) BmAny(relativePath string, handlerFunc interface{}) IBmRoutes {
	group.BmHandle(http.MethodGet, relativePath, handlerFunc)
	group.BmHandle(http.MethodPost, relativePath, handlerFunc)
	group.BmHandle(http.MethodPut, relativePath, handlerFunc)
	group.BmHandle(http.MethodPatch, relativePath, handlerFunc)
	group.BmHandle(http.MethodHead, relativePath, handlerFunc)
	group.BmHandle(http.MethodOptions, relativePath, handlerFunc)
	group.BmHandle(http.MethodDelete, relativePath, handlerFunc)
	group.BmHandle(http.MethodConnect, relativePath, handlerFunc)
	group.BmHandle(http.MethodTrace, relativePath, handlerFunc)
	return group
}

func (group *BmRouterGroup) BmApi(relativePath string, handlerStruct interface{}) IBmRoutes {
	value := reflect.ValueOf(handlerStruct)
	indirect := reflect.Indirect(value).Type()
	vType := value.Type()
	//必须是指针类型的struct对象
	isHandlerPtr := value.Kind() == reflect.Ptr
	if !(isHandlerPtr && value.Elem().Kind() == reflect.Struct) {
		if !isHandlerPtr {
			panic("BmApi路由必须是指针类型的struct对象, 当前为：" + value.Kind().String())
		} else {
			panic("BmApi路由必须是指针类型的struct对象, 当前为非指针：" + value.Elem().Kind().String())
		}
	}

	apiPath := indirect.PkgPath()
	apiName := indirect.Name()
	mNum := value.NumMethod()

	for i := 0; i < mNum; i++ {
		vTypeMethod := vType.Method(i)
		method := value.Method(i)

		mName := vTypeMethod.Name
		docModel := GetDocModel(apiPath, apiName+"."+mName)
		//fmt.Println("docModel:", apiPath,docModel)
		if docModel == nil {
			continue
		}

		for _, httpMethod := range docModel.Methods {
			//fmt.Println("index:", httpMethod,i, mName, method.Type().NumIn(), value.Method(i).Type().NumOut())
			group.handle(httpMethod, relativePath+docModel.RouterPath, method, vTypeMethod)
		}
	}

	return group
}

// BmGet is a shortcut for router.Handle("GET", path, handle).
func (group *BmRouterGroup) BmGet(relativePath string, handlerFunc interface{}) IBmRoutes {
	return group.BmHandle(http.MethodGet, relativePath, handlerFunc)
}

// BmPost is a shortcut for router.Handle("POST", path, handle).
func (group *BmRouterGroup) BmPost(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodPost, relativePath, handlers)
}

// BmDelete is a shortcut for router.Handle("DELETE", path, handle).
func (group *BmRouterGroup) BmDelete(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodDelete, relativePath, handlers)
}

// BmPatch is a shortcut for router.Handle("PATCH", path, handle).
func (group *BmRouterGroup) BmPatch(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodPatch, relativePath, handlers)
}

// BmPut is a shortcut for router.Handle("PUT", path, handle).
func (group *BmRouterGroup) BmPut(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodPut, relativePath, handlers)
}

// BmOptions is a shortcut for router.Handle("OPTIONS", path, handle).
func (group *BmRouterGroup) BmOptions(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodOptions, relativePath, handlers)
}

// BmHead is a shortcut for router.Handle("HEAD", path, handle).
func (group *BmRouterGroup) BmHead(relativePath string, handlers interface{}) IBmRoutes {
	return group.BmHandle(http.MethodHead, relativePath, handlers)
}
