package bm

//检查字符串是否存在切片中
func SliceExistStr(target string, str_array []string) bool {
	for _, element := range str_array {
		if target == element {
			return true
		}
	}
	return false
}
