package bm

import (
	"bufio"
	"errors"
	"github.com/gin-gonic/gin"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
)

var (
	// GlobalDocModelPathMap [path]:[[name]:[model]]
	GlobalDocModelPathMap = make(map[string]map[string]*DocModel)
	// GlobalDocModelMap [path+name]:[model]
	GlobalDocModelMap = make(map[string]*DocModel)
)

var _mu sync.Mutex // protects the serviceMap

// FieldInfo 参数信息
type FieldInfo struct {
	Name string // 参数名
	// URL      string      // web 访问参数
	Tag      string      // 标签
	Type     string      // 类型
	TypeRef  *StructInfo // 类型定义
	IsArray  bool        // 是否是数组
	Requierd bool        // 是否必须
	Note     string      // 注释
	Default  string      // 默认值
}

// StructInfo struct define
type StructInfo struct {
	Items []FieldInfo // 结构体元素
	Note  string      // 注释
	Name  string      //结构体名字
	Pkg   string      // 包名
}

// DocModel model
type DocModel struct {
	Title      string
	RouterPath string
	PkgName    string
	Name       string
	Methods    []string
	Note       string
	Req, Resp  []FieldInfo
}

// registerHandlerObj Multiple registration methods.获取并过滤要绑定的参数
func registerHandlerObj(router gin.IRoutes, httpMethod []string, relativePath string) {
	for _, v := range httpMethod {
		println(v)
		//switch strings.ToUpper(v) {
		//case "POST":
		//	router.POST(relativePath, call)
		//case "GET":
		//	router.GET(relativePath, call)
		//case "DELETE":
		//	router.DELETE(relativePath, call)
		//case "PATCH":
		//	router.PATCH(relativePath, call)
		//case "PUT":
		//	router.PUT(relativePath, call)
		//case "OPTIONS":
		//	router.OPTIONS(relativePath, call)
		//case "HEAD":
		//	router.HEAD(relativePath, call)
		//case "ANY":
		//	router.Any(relativePath, call)
		//}
	}
}

/**
 * 获取指定目录下文件
 */
func getPkgGoFiles(path string, mode parser.Mode) (map[string]*ast.File, *token.FileSet) {
	fileSet := token.NewFileSet()
	pkgs, _ := parser.ParseDir(fileSet, path, nil, mode)
	for _, pkg := range pkgs {
		return pkg.Files, fileSet
	}
	return nil, fileSet
}

func getDocModel(file *ast.File, fileSet *token.FileSet) (docStructs map[string]*DocModel, docMethods map[string]*DocModel) {
	// find all exported func of sturct objName
	pkgName := file.Name.Name
	//fmt.Println(file.Name.String())
	docStructs = make(map[string]*DocModel)
	docMethods = make(map[string]*DocModel)
	for _, d := range file.Decls {
		switch specDecl := d.(type) {
		case *ast.GenDecl:
			//ast.Print(fileSet, specDecl.Specs)
			doc := parserStruct(specDecl)
			if doc != nil {
				doc.PkgName = pkgName
				docStructs[doc.Name] = doc
			}
		case *ast.FuncDecl:
			doc := parserFunc(specDecl) //解析函数
			if doc != nil {
				//ast.Print(fileSet, specDecl)
				doc.PkgName = pkgName
				doc.Name = specDecl.Name.Name
				if specDecl.Recv != nil {
					ast.Inspect(specDecl.Recv.List[0].Type, func(n ast.Node) bool {
						switch x := n.(type) {
						case *ast.Ident:
							doc.Name = x.Name + "." + doc.Name
						}
						return true
					})
				}
				docMethods[doc.Name] = doc

				//匿名函数集合
				funcLits := parserFuncLit(specDecl.Body.List, fileSet)
				for key, val := range funcLits {
					docMethods[doc.Name+".func"+key] = val
					//fmt.Println("parserFuncLit", doc.Name+".func"+key)
				}

			}
		}
	}

	return docStructs, docMethods
}

var routeRegex = regexp.MustCompile(`@Router\s+(\S+)(?:\s+\[(\S+)\])?`)

//分析匿名函数
func parserFuncLit(bodyList []ast.Stmt, fileSet *token.FileSet) map[string]*DocModel {
	//ast.Print(fileSet, bodyList)
	docs := make(map[string]*DocModel)

	funcLitNum := 0 //匿名函数计数
	for _, stmt := range bodyList {
		switch stmtDeck := stmt.(type) {
		case *ast.ExprStmt:
			callExpr := stmtDeck.X.(*ast.CallExpr)
			if _, ok := callExpr.Fun.(*ast.FuncLit); ok {
				funcLitNum++
			}

			if _, ok := callExpr.Fun.(*ast.SelectorExpr); ok {
				for _, arg := range callExpr.Args {
					if funcLit, fOk := arg.(*ast.FuncLit); fOk {
						ftype := funcLit.Type
						if ftype != nil {
							funcLitNum++
							//fmt.Println(funcLitNum, dm)
							//ast.Print(fileSet, ftype)
							docs[strconv.Itoa(funcLitNum)] = parserFuncType(ftype, nil)
						}
					}
				}
			}
		case *ast.BlockStmt:
			var args []ast.Expr
			for _, exprStmt := range stmtDeck.List {
				if callExpr, ok := exprStmt.(*ast.ExprStmt); ok {
					args = append(args, callExpr.X.(*ast.CallExpr).Args...)
				}
			}

			for _, arg := range args {
				argType, ok := arg.(*ast.FuncLit)
				if !ok {
					continue
				}
				ftype := argType.Type
				if ftype != nil {
					funcLitNum++
					//fmt.Println(funcLitNum, dm)
					docs[strconv.Itoa(funcLitNum)] = parserFuncType(ftype, nil)
				}
			}
		}
	}

	return docs
}

//解析func类型
func parserFuncType(ftype *ast.FuncType, dm *DocModel) *DocModel {
	if dm == nil {
		dm = &DocModel{}
	}
	if ftype == nil {
		return dm
	}
	if ftype.Params != nil {
		req := make([]FieldInfo, ftype.Params.NumFields())
		reqIndex := 0
		for _, field := range ftype.Params.List {
			for _, n := range field.Names {
				req[reqIndex] = FieldInfo{Name: n.Name, Type: parserAstExprType(field.Type)}
				reqIndex++
			}
		}
		//fmt.Println(req)
		dm.Req = req
	}

	if ftype.Results != nil {
		resp := make([]FieldInfo, ftype.Results.NumFields())
		respIndex := 0
		for _, field := range ftype.Results.List {
			for _, n := range field.Names {
				resp[respIndex] = FieldInfo{Name: n.Name, Type: parserAstExprType(field.Type)}
				respIndex++
			}
		}
		dm.Resp = resp
	}
	return dm
}

//分析ast.Expr，得到类型名称
func parserAstExprType(expr ast.Expr) string {
	switch exprType := expr.(type) {
	case *ast.SelectorExpr:
		if x, ok := exprType.X.(*ast.Ident); ok {
			return x.Name + "." + exprType.Sel.Name
		}
	case *ast.Ident:
		return exprType.Name
	}
	return ""
}
func parserFunc(f *ast.FuncDecl) *DocModel {
	dm := DocModel{}
	if f.Doc != nil {
		//注释
		for _, c := range f.Doc.List {
			t := strings.TrimSpace(strings.TrimPrefix(c.Text, "//"))
			if strings.HasPrefix(t, "@Title") {
				dm.Title = strings.TrimPrefix(t, "@Title ")
			} else if strings.HasPrefix(t, "@Router ") {
				matches := routeRegex.FindStringSubmatch(t)
				if len(matches) == 3 {
					//避免有的加斜杠，有的未加造成bug，所以前后都清完在加
					dm.RouterPath = "/" + strings.Trim(matches[1], "/")
					methods := matches[2]
					if methods == "" {
						dm.Methods = []string{"GET"}
					} else {
						methods = strings.ToUpper(methods)
						dm.Methods = strings.Split(methods, ",")
					}

				} else {
					//return nil, errors.New("Router information is missing")
				}
			}
		}
	}

	return parserFuncType(f.Type, &dm)
}

func parserStruct(f *ast.GenDecl) *DocModel {
	dm := DocModel{}
	if f.Doc != nil {
		for _, c := range f.Doc.List {
			t := strings.TrimSpace(strings.TrimPrefix(c.Text, "//"))
			if strings.HasPrefix(t, "@Title") {
				dm.Title = strings.TrimPrefix(t, "@Title ")
			}
		}
	}
	return &dm
}

//得到路径下文件map
func MapDocModel(apiPath string) (mapDocModels map[string]*DocModel) {
	mapDocModels = make(map[string]*DocModel)
	firstIndex := strings.Index(apiPath, "/")
	if firstIndex > -1 {
		apiPath = apiPath[firstIndex+1:]
	}

	//根目录特殊处理
	if apiPath == "main" {
		apiPath = "./"
	} else {
		files, modFile, isFind := GetModuleInfo(2)
		if !isFind {
			return mapDocModels
		}
		if apiPath == files {
			apiPath = modFile
		}
	}

	files, fileSet := getPkgGoFiles(apiPath, parser.ParseComments)
	//fmt.Println(apiPath,len(files))
	for _, fl := range files {
		_, dms := getDocModel(fl, fileSet)
		//fmt.Println(len(dms))
		for key, val := range dms {
			//fmt.Println("dms", key)
			mapDocModels[key] = val
		}
	}
	return mapDocModels
}

//得到对应方法的DocModel
func GetDocModel(path string, name string) *DocModel {
	//fmt.Println(path, name)
	if path == "" || name == "" {
		return nil
	}
	_mu.Lock()
	defer _mu.Unlock()

	apiModel := GlobalDocModelPathMap[path]
	if apiModel == nil || len(apiModel) == 0 {
		apiModel = MapDocModel(path)
		if apiModel != nil && len(apiModel) > 0 {
			GlobalDocModelPathMap[path] = apiModel
		}
	}

	if apiModel != nil && len(apiModel) > 0 {
		return apiModel[name]
	}
	return nil
}

// GetModuleInfo find and get module info , return module [ name ,path ]
// 通过model信息获取[model name] [和 根目录绝对地址]
func GetModuleInfo(n int) (string, string, bool) {
	index := n
	// This is used to support third-party package encapsulation
	// 这样做用于支持第三方包封装,(主要找到main调用者)
	for true { // find main file
		_, filename, _, ok := runtime.Caller(index)
		if ok {
			if strings.HasSuffix(filename, "runtime/asm_amd64.s") {
				index = index - 2
				break
			}
			index++
		} else {
			panic(errors.New("package parsing failed:can not find main files"))
		}
	}

	_, filename, _, _ := runtime.Caller(index)
	filename = strings.Replace(filename, "\\", "/", -1) // offset
	for true {
		n := strings.LastIndex(filename, "/")
		if n > 0 {
			filename = filename[0:n]
			if CheckFileIsExist(filename + "/go.mod") {
				list := ReadFile(filename + "/go.mod")
				if len(list) > 0 {
					line := strings.TrimSpace(list[0])
					if len(line) > 0 && strings.HasPrefix(line, "module") { // find it
						return strings.TrimSpace(strings.TrimPrefix(line, "module")), filename, true
					}
				}
			}
		} else {
			break
			// panic(errors.New("package parsing failed:can not find module file[go.mod] , golang version must up 1.11"))
		}
	}

	// never reach
	return "", "", false
}

// CheckFileIsExist 检查目录是否存在
func CheckFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

// ReadFile 读取文件
func ReadFile(fname string) (src []string) {
	f, err := os.OpenFile(fname, os.O_RDONLY, 0666)
	if err != nil {
		return []string{}
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, _, err := rd.ReadLine()
		if err != nil || io.EOF == err {
			break
		}
		src = append(src, string(line))
	}

	return src
}
